﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Logique d'interaction pour GlobalNotification.xaml
    /// </summary>
    public partial class GlobalNotification : UserControl
    {
        private Point _position;
        public bool IsOpen = false;

        public GlobalNotification()
        {
            InitializeComponent();
        }

        public void ShowNotification()
        {
            Storyboard storyboard = (Storyboard) FindResource("OpenAnimation");
            Storyboard.SetTarget(storyboard, Border);
            storyboard.Begin();
            IsOpen = true;
        }

        public void HideNotification()
        {
            Storyboard storyboard = (Storyboard) FindResource("CloseAnimation");
            Storyboard.SetTarget(storyboard, Border);
            storyboard.Begin();
            IsOpen = false;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            _position = TranslatePoint(new Point(0,0), (UIElement)Parent);
        }

        private void Grid_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            if (mainWindow != null)
            {
                mainWindow.OpenMenu(new Matrix(), MainMenu.SubMenu.NotificationList);
            }
            e.Handled = true;
        }

        private void Grid_OnTouchUp(object sender, TouchEventArgs e)
        {
            MainWindow mainWindow = (MainWindow)Application.Current.MainWindow;
            if (mainWindow != null)
            {
                mainWindow.OpenMenu(new Matrix(), MainMenu.SubMenu.NotificationList);
            }
            e.Handled = true;
        }
    }
}
