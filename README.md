# Vous avez ajouté votre premier fichier ReadMe.
Un fichier README.md est destiné à orienter rapidement les lecteurs vers ce que votre projet peut faire. Vous découvrez Markdown ? [En savoir plus](http://go.microsoft.com/fwlink/p/?LinkId=524306&clcid=0x409)

## Modifiez ce fichier ReadMe et validez vos changements dans une branche de rubrique
Dans Git, les branches ne vous coûtent rien. Utilisez-les chaque fois que vous apportez des changements à votre référentiel. Modifiez ce fichier en cliquant sur l'icône de modification.

Apportez ensuite des changements à ce fichier ReadMe.

> Apportez des **modifications** à _this_ blockquote

Une fois que vous avez fini, cliquez sur la flèche déroulante vers le bas en regard du bouton d'enregistrement. Cela vous permet de valider vos modifications dans une nouvelle branche.

## Créez une requête de tirage pour ajouter vos changements à la branche maître
Les requêtes de tirage permettent de transférer vos modifications d'une branche de rubrique vers la branche maître.

Cliquez sur la page **Requêtes de tirage** dans le hub **CODE**, puis cliquez sur Nouvelle requête de tirage pour créer une requête de tirage de votre branche de rubrique vers la branche maître.

Une fois que vous avez fini d'ajouter les informations, cliquez sur Créer la requête de tirage. Une fois qu'une requête de tirage est envoyée, les réviseurs peuvent voir vos changements, recommander des modifications ou même proposer des validations de suivi via une transmission de type push.

Première création de requête de tirage ? [En savoir plus](http://go.microsoft.com/fwlink/?LinkId=533211&clcid=0x409)

### Félicitations ! Vous avez fait le tour du hub de code.

# Étapes suivantes

Si vous ne l'avez pas encore fait :
* [Installer Visual Studio](http://go.microsoft.com/fwlink/?LinkId=309297&clcid=0x409&slcid=0x409)
* [Installer Git](http://git-scm.com/downloads)

Clonez ensuite ce référentiel sur votre ordinateur local pour démarrer votre propre projet.

Codez bien !