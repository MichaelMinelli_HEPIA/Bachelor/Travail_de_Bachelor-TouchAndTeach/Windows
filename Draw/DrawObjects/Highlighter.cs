﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Globalization;

namespace TouchAndTeachWindows.Draw.DrawObjects
{
    [JsonObject(MemberSerialization.OptIn)]
    class Highlighter : DrawObject
    {
        
        //private readonly List<Point> _pointList;

        [JsonProperty("PointListDic")]
        private Dictionary<int, List<Point>> _pointListDic;

        private GeometryDrawing _shape;
        private PathGeometry _path;
        private Dictionary<int, PathFigure> _figure;

        [JsonProperty("Painter")]
        public Painter Painter { get; set; }

        private Dictionary<int, Point> _lastPoint, _endPoint;
        private QuadraticBezierSegment _bezierSegment;

        public Highlighter(Painter painter) : this()
        {
            Uuid = Utils.Utils.GenerateUuid();
            Painter = painter;
            //_pointList = new List<Point>();
            _pointListDic = new Dictionary<int, List<Point>>();
        }

        public Highlighter() : base()
        {
            ObjectType = typeof(Highlighter).Name;
            _lastPoint = new Dictionary<int, Point>();
            _endPoint = new Dictionary<int, Point>();

            _figure = new Dictionary<int, PathFigure>();
            _path = new PathGeometry();
        }

        public Highlighter(Painter painter, Dictionary<int, PathFigure> figures, Matrix matrixTransform) : this(painter)
        {
            _figure = figures;
            _path = new PathGeometry();

            foreach (var fig in _figure)
                _path.Figures.Add(fig.Value);

            this.MatrixTransform = matrixTransform;

            _shape = new GeometryDrawing
            {
                Pen =
                        new Pen(new SolidColorBrush(Color.FromArgb(128, Painter.Color.Color.R, Painter.Color.Color.G, Painter.Color.Color.B)), Painter.StrokeWidth)
                        {
                            DashCap = PenLineCap.Round,
                            EndLineCap = PenLineCap.Round,
                            LineJoin = PenLineJoin.Round,
                            StartLineCap = PenLineCap.Round
                        }
            };
            _shape.Geometry = _path;
        }

        public override void AddToDrawingContext(DrawingContext drawingContext)
        {
            drawingContext.PushTransform(new MatrixTransform(MatrixTransform));
            drawingContext.DrawDrawing(_shape);
        }

        public override void AddPoint(Point pt, int? id = null)
        {
            PointToPath(pt, id ?? 0);

            if (!_pointListDic.ContainsKey(id ?? 0))
                _pointListDic.Add(id ?? 0, new List<Point>());

            _pointListDic[id ?? 0].Add(pt);
            //_pointList.Add(pt);
        }

        public void AddNewStroke(Point pt, int id)
        {
            _lastPoint.Add(id, pt);
            _endPoint.Add(id, new Point());

            _figure.Add(id, new PathFigure { StartPoint = pt });
            _path.Figures.Add(_figure[id]);
        }

        public void PointToPath(Point pt, int id)
        {
            if (_shape == null)
            {
                _shape = new GeometryDrawing
                {
                    Pen =
                        new Pen(new SolidColorBrush(Color.FromArgb(128, Painter.Color.Color.R, Painter.Color.Color.G, Painter.Color.Color.B)), Painter.StrokeWidth)
                        {
                            DashCap = PenLineCap.Round,
                            EndLineCap = PenLineCap.Round,
                            LineJoin = PenLineJoin.Round,
                            StartLineCap = PenLineCap.Round
                        }
                };
                _shape.Geometry = _path;
            }
            else
            {
                _endPoint[id] = new Point((pt.X + _lastPoint[id].X) / 2, (pt.Y + _lastPoint[id].Y) / 2);
                _bezierSegment = new QuadraticBezierSegment(_lastPoint[id], _endPoint[id], true);
                _figure[id].Segments.Add(_bezierSegment);
                _lastPoint[id] = pt;
            }
        }

        /// <summary>
        /// Remove last draw
        /// </summary>
        /// <returns>return true if there is still other stroke, false if it was the last one</returns>
        public bool RemoveLastDraw()
        {
            int toRemove = _figure.Keys.Last();

            return RemoveDraw(toRemove);
        }

        /// <summary>
        /// Remove specific draw
        /// </summary>
        /// <param name="figureId">Id of the figure to remove</param>
        /// <returns>return true if there is still other stroke, false if it was the last one</returns>
        public bool RemoveDraw(int figureId)
        {
            _path.Figures.Remove(_figure[figureId]);
            _figure.Remove(figureId);

            //_lastPoint.Remove(figureId);
            //_endPoint.Remove(figureId);

            return _figure.Count > 0;
        }

        public override Rect? GetBounds()
        {
            if (_path == null) return null;

            Rect bound = _path.Bounds;
            if (!bound.IsEmpty)
                bound.Inflate(Painter.StrokeWidth / 2, Painter.StrokeWidth / 2);
            bound.Transform(MatrixTransform);
            return bound;
        }

        public override Geometry GetGeometry()
        {
            return new PathGeometry(_figure.Values, FillRule.EvenOdd, new MatrixTransform(MatrixTransform));
        }

        public Dictionary<int, PathGeometry> GetFiguresGeometry(bool withTransform = true)
        {
            return _figure.Select(f =>
                new KeyValuePair<int, PathGeometry>(
                    f.Key,
                    new PathGeometry(new List<PathFigure> { f.Value }, FillRule.EvenOdd, withTransform ? new MatrixTransform(MatrixTransform) : null)))
                    .ToDictionary(keyVal => keyVal.Key, keyVal => keyVal.Value);
        }

        
        public Dictionary<int, PathFigure> GetPathFigures(List<int> listIds)
        {
            return _figure.Where(f => listIds.Contains(f.Key))
                    .ToDictionary(keyVal => keyVal.Key, keyVal => keyVal.Value);
        }


        public override XElement GetSVG(XNamespace xmlns, Matrix transform)
        {
            XElement group = new XElement(xmlns + "g");

            Matrix svgMatrix = MatrixTransform.Clone();
            svgMatrix.Append(transform);

            foreach (var pointList in _pointListDic.Values)
            {
                StringBuilder path = new StringBuilder(string.Format(CultureInfo.InvariantCulture, "M {0} {1} ", pointList[0].X, pointList[0].Y));

                for (int i = 1; i < pointList.Count; i++)
                    path.AppendFormat(CultureInfo.InvariantCulture, "Q {0} {1} {2} {3} ",
                        pointList[i - 1].X,
                        pointList[i - 1].Y,
                        (pointList[i].X + pointList[i - 1].X) / 2,
                        (pointList[i].Y + pointList[i - 1].Y) / 2);

                group.Add(new XElement(xmlns + "path",
                    new XAttribute("fill", "none"),
                    new XAttribute("stroke", string.Format(CultureInfo.InvariantCulture, "rgb({0},{1},{2})", Painter.Color.Color.R, Painter.Color.Color.G, Painter.Color.Color.B)),
                    new XAttribute("stroke-width", Painter.StrokeWidth.ToString()),
                    new XAttribute("stroke-linecap", "round"),
                    new XAttribute("stroke-opacity", "0.5"),
                    new XAttribute("transform",
                        string.Format(CultureInfo.InvariantCulture, "matrix({0},{1},{2},{3},{4},{5})",
                            svgMatrix.M11,
                            svgMatrix.M12,
                            svgMatrix.M21,
                            svgMatrix.M22,
                            svgMatrix.OffsetX,
                            svgMatrix.OffsetY)),
                    new XAttribute("d", path.ToString())
                    ));
            }

            return group;
        }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            foreach (var ptDic in _pointListDic)
            {
                AddNewStroke(ptDic.Value.First(), ptDic.Key);

                foreach (Point pt in ptDic.Value.Skip(1))
                    PointToPath(pt, ptDic.Key);
            }
        }
    }
}
