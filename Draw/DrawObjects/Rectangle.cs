﻿using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Globalization;

namespace TouchAndTeachWindows.Draw.DrawObjects
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Rectangle : DrawObject
    {
        private static readonly Point InvalidPoint = new Point(-1, -1);
        private GeometryDrawing _shape;

        [JsonProperty("Start")]
        private Point _start = InvalidPoint;

        [JsonProperty("Stop")]
        private Point _stop = InvalidPoint;

        private bool _startPoint = true;
        private PathGeometry _path;

        [JsonProperty("Painter")]
        public Painter Painter { get; set; }

        public Rectangle(Painter painter) : this()
        {
            Uuid = Utils.Utils.GenerateUuid();
            Painter = painter;
        }

        public Rectangle() : base()
        {
            ObjectType = typeof (Rectangle).Name;
        }

        public override void AddToDrawingContext(DrawingContext drawingContext)
        {
            drawingContext.PushTransform(new MatrixTransform(MatrixTransform));
            drawingContext.DrawDrawing(_shape);
        }

        public override void AddPoint(Point pt, int? id = null)
        {
            if (_startPoint)
            {
                _startPoint = false;
                _start = pt;
                InitPath();
            }
            else
            {
                _stop = pt;
                WritePath();
            }
        }

        public override Rect? GetBounds()
        {
            if (_path == null) return null;

            Rect bound = _path.Bounds;
            if (!bound.IsEmpty)
                bound.Inflate(Painter.StrokeWidth / 2, Painter.StrokeWidth / 2);
            bound.Transform(MatrixTransform);
            return bound;
        }

        private void InitPath()
        {
            if (_shape != null) return;

            _path = new PathGeometry();
            _shape = new GeometryDrawing
            {
                Pen =
                    new Pen(Painter.Color, Painter.StrokeWidth)
                    {
                        DashCap = PenLineCap.Round,
                        EndLineCap = PenLineCap.Round,
                        LineJoin = PenLineJoin.Round,
                        StartLineCap = PenLineCap.Round
                    },
                Geometry = _path
            };
        }

        private void WritePath()
        {
            GeometryGroup geometryGroup = new GeometryGroup();
            geometryGroup.Children.Add(new RectangleGeometry(new Rect(_start, _stop)));
            _path.Clear();
            _path.AddGeometry(geometryGroup);
        }

        public override Geometry GetGeometry()
        {
            return new RectangleGeometry(new Rect(_start, _stop), 0, 0, new MatrixTransform(MatrixTransform));
        }

        public override XElement GetSVG(XNamespace xmlns, Matrix transform)
        {
            Matrix svgMatrix = MatrixTransform.Clone();
            svgMatrix.Append(transform);

            Rect rectangle = new Rect(_start, _stop);

            return new XElement(xmlns + "rect",
                new XAttribute("stroke", string.Format(CultureInfo.InvariantCulture, "rgb({0},{1},{2})", Painter.Color.Color.R, Painter.Color.Color.G, Painter.Color.Color.B)),
                new XAttribute("stroke-width", Painter.StrokeWidth.ToString()),
                new XAttribute("fill", "none"),
                new XAttribute("rx", (Painter.StrokeWidth / 8f).ToString(CultureInfo.InvariantCulture)),
                new XAttribute("x", rectangle.X.ToString(CultureInfo.InvariantCulture)),
                new XAttribute("y", rectangle.Y.ToString(CultureInfo.InvariantCulture)),
                new XAttribute("width", rectangle.Width.ToString(CultureInfo.InvariantCulture)),
                new XAttribute("height", rectangle.Height.ToString(CultureInfo.InvariantCulture)),
                new XAttribute("transform",
                    string.Format(CultureInfo.InvariantCulture,
                        "matrix({0},{1},{2},{3},{4},{5})",
                        svgMatrix.M11,
                        svgMatrix.M12,
                        svgMatrix.M21,
                        svgMatrix.M22,
                        svgMatrix.OffsetX,
                        svgMatrix.OffsetY))
                );
        }

        [OnDeserialized]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            InitPath();
            WritePath();
        }
    }
}
