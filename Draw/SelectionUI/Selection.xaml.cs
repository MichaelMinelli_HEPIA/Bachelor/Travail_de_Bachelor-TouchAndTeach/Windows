﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TouchAndTeachWindows.Comm;
using TouchAndTeachWindows.Draw.DrawObjects;

namespace TouchAndTeachWindows.Draw.SelectionUI
{
    /// <summary>
    /// Interaction logic for Selection.xaml
    /// </summary>
    public partial class Selection : UserControl, IDisposable
    {

        private List<DrawObject> _selectedObjects = null;
        private GeometryDrawing _transparentRectangle;
        private Rect _selectionRect;

        private Boolean _manipulated = false;
        private Matrix _manipulationMatrix;
        private Point? _mouseLastPos;

        public Selection()
        {
            InitializeComponent();
        }

        public Selection(List<DrawObject> drawObjects) : this()
        {
            _selectedObjects = drawObjects;
            _selectionRect = Rect.Empty;
            foreach (DrawObject obj in drawObjects)
            {
                if (_selectionRect.IsEmpty)
                {
                    _selectionRect = obj.GetBounds().Value;
                    continue;
                }

                _selectionRect.Union(obj.GetBounds().Value);
            }

            Width = _selectionRect.Width + 50;
            Height = _selectionRect.Height + 50;
            _transparentRectangle = new GeometryDrawing(Brushes.Transparent, null, new RectangleGeometry(new Rect(0,0, Width, Height)));;
            _manipulationMatrix.Translate(_selectionRect.Left - 25, _selectionRect.Top - 25);
            RenderTransform = new MatrixTransform(_manipulationMatrix);

            ShowDrawing();
        }

        private void ShowDrawing()
        {
           DrawingGroup drawingGroup = new DrawingGroup();
            if (_transparentRectangle == null) return;

            drawingGroup.Children.Add(_transparentRectangle);

            RenderTargetBitmap bitmap = new RenderTargetBitmap((int)Width, (int)Height, 96, 96, PixelFormats.Pbgra32);
            DrawingVisual drawingVisual = new DrawingVisual();

            Matrix contentMatrix = Matrix.Identity;
            contentMatrix.Translate(-_selectionRect.Left + 25, -_selectionRect.Top + 25);

            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {

                //_contentMatrix = new Matrix();
                //_contentMatrix.Translate(pos.X, pos.Y);
                //_contentMatrix.Scale(_scale, _scale);
                //_contentMatrix.ScaleAt(0.7, 0.7, PostitSize / 2, PostitSize / 2);
                drawingContext.PushTransform(new MatrixTransform(contentMatrix));

                foreach (DrawObject drawObject in _selectedObjects)
                {
                    if (drawObject != null)
                    {
                        drawObject.AddToDrawingContext(drawingContext);
                        drawingContext.Pop();
                    }
                }
            }
            bitmap.Render(drawingVisual);
            RenderedImage.Source = bitmap;
        }

        private void UserControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _mouseLastPos = e.GetPosition((Panel)Parent);
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _mouseLastPos = null;
        }

        private void UserControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (_mouseLastPos == null) return;

            Point newPos = e.GetPosition((Panel)Parent);
            _manipulationMatrix.Translate(newPos.X - _mouseLastPos.Value.X, newPos.Y - _mouseLastPos.Value.Y);
            RenderTransform = new MatrixTransform(_manipulationMatrix);
            _mouseLastPos = newPos;
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            _mouseLastPos = null;
        }

        private void UserControl_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (_mouseLastPos != null ||
                (!_manipulated &&
                (!(e.DeltaManipulation.Translation.LengthSquared > 50)) &&
                (!(Math.Abs(e.DeltaManipulation.Scale.LengthSquared - 2) > double.Epsilon)) &&
                (!(Math.Abs(e.DeltaManipulation.Rotation) > double.Epsilon)))) return;

            _manipulated = true;
            _manipulationMatrix.RotateAt(e.DeltaManipulation.Rotation, e.ManipulationOrigin.X, e.ManipulationOrigin.Y);

            _manipulationMatrix.ScaleAt(e.DeltaManipulation.Scale.X, e.DeltaManipulation.Scale.Y, e.ManipulationOrigin.X, e.ManipulationOrigin.Y);
            _manipulationMatrix.Translate(e.DeltaManipulation.Translation.X, e.DeltaManipulation.Translation.Y);

            RenderTransform = new MatrixTransform(_manipulationMatrix);
        }

        private void UserControl_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            _manipulated = false;
            e.Handled = true;
        }

        public void MergeSelection()
        {
            DrawController controller = DrawController.GetDrawController();
            Matrix matrix = new Matrix();
            matrix.Translate(-_selectionRect.Left + 25, -_selectionRect.Top + 25);
            matrix.Append(_manipulationMatrix);

            foreach (DrawObject drawObject in _selectedObjects)
            {
                if (drawObject != null)
                {
                    drawObject.MatrixTransform.Append(matrix);
                    controller.AddDrawObjectToCurrentPage(drawObject);
                }
            }

            Dispose();
        }

        public void Dispose()
        {
            Panel panel = (Panel)Parent;
            if (panel != null)
            {
                UIElementCollection children = panel.Children;
                if (children != null)
                    children.Remove(this);
            }
            _transparentRectangle = null;
            _mouseLastPos = null;
            _selectedObjects.Clear();
        }

        private void OpenMenu(Point position)
        {
            var menu = new SelectionMenu();
            menu.ActionActivated += menu_ActionActivated;
            Matrix menuMatrix = Matrix.Identity;
            menuMatrix.Translate(position.X - 110, position.Y - 97.5);
            Matrix inverse = _manipulationMatrix;
            inverse.Invert();
            menuMatrix.Append(inverse);
            menu.RenderTransform = new MatrixTransform(menuMatrix);
            MenuCanvas.Children.Add(menu);
        }

        void menu_ActionActivated(SelectionMenu menu, SelectionMenuAction action)
        {
            MenuCanvas.Children.Clear();

            switch (action)
            {
                case SelectionMenuAction.Merge:
                    MergeSelection();
                    break;
                case SelectionMenuAction.Close:
                    Dispose();
                    break;
                case SelectionMenuAction.AvailableDraw:
                    AvailableDrawsManager.GetAvailableDrawsManager().AddDraw(new AvailableDraw(_selectedObjects));
                    MergeSelection();
                    break;
                default:
                    break;
            }
        }

        private void StackPanel_TouchDown(object sender, TouchEventArgs e)
        {
            OpenMenu(e.GetTouchPoint((FrameworkElement)this.Parent).Position);
        }

        private void StackPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            OpenMenu(e.GetPosition((FrameworkElement)this.Parent));
        }
    }
}
