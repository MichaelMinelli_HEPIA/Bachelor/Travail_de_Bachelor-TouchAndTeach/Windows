﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TouchAndTeachWindows.Draw.DrawObjects;
using System.Linq;

namespace TouchAndTeachWindows.Draw
{
    /// <summary>
    /// Interaction logic for ThumbnailUC.xaml
    /// </summary>
    public partial class Thumbnail : UserControl
    {
        #region Events Declaration
        public static readonly RoutedEvent PressedEvent = EventManager.RegisterRoutedEvent("ThumbnailPressed",
            RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(Thumbnail));

        public event RoutedEventHandler Pressed
        {
            add { AddHandler(PressedEvent, value); }
            remove { RemoveHandler(PressedEvent, value); }
        }

        private readonly RoutedEventArgs _pressedArgs = new RoutedEventArgs(PressedEvent);
        #endregion

        private readonly Page _page;
        private double _scale;
        private const int ThumbSize = 80;

        private int _originalWidth, _originalHeight;
        private RenderTargetBitmap _bitmap;

        public Thumbnail(Page page, int originalWidth, int originalHeight)
        {
            InitializeComponent();
            _originalWidth = originalWidth;
            _originalHeight = originalHeight;

            Image.VerticalAlignment = VerticalAlignment.Center;
            Image.HorizontalAlignment = HorizontalAlignment.Center;
            Image.Source = _bitmap;
            _page = page;
            Text.Content = page.Title;
            _scale = Border.Width / originalWidth;
            DrawThumb();
        }

        public ImageSource GetBitmap()
        {
            return _bitmap;
        }

        public void Refresh()
        {
            DrawThumb();
        }

        private void DrawThumb()
        {
            if (_page != null)
            {
                Rect? bound = _page.GetBoundingBox();
                if (bound == null) return;
                Point pos = new Point();

                if (bound.Value.Width < bound.Value.Height)
                    _scale = (ThumbSize / bound.Value.Height) * 0.8;
                else
                    _scale = (ThumbSize / bound.Value.Width) * 0.8;
                pos = new Point((ThumbSize / _scale - bound.Value.Width) / 2 - bound.Value.Left,
                    (ThumbSize / _scale - bound.Value.Height) / 2 - bound.Value.Top);

                _bitmap = new RenderTargetBitmap(ThumbSize, ThumbSize, 96, 96, PixelFormats.Pbgra32);
                DrawingVisual drawingVisual = new DrawingVisual();
                //GeometryDrawing transparentRectangle = new GeometryDrawing(Brushes.Transparent, null, new RectangleGeometry(bound.Value));

                using (DrawingContext drawingContext = drawingVisual.RenderOpen())
                {
                    //drawingContext.DrawDrawing(transparentRectangle);
                    Matrix matrix = new Matrix();
                    matrix.Translate(pos.X, pos.Y);
                    matrix.Scale(_scale, _scale);
                    drawingContext.PushTransform(new MatrixTransform(matrix));

                    DrawObject background = _page.Background;
                    if (background != null && _page.FirstBackgroundResizeDone/*&& !(background is DrawPDF)*/)
                    {

                        background.AddToDrawingContext(drawingContext);
                        drawingContext.Pop();
                    }

                    foreach (DrawObject drawObject in _page.GetAllDrawObjects().Where(o => !(o is DrawSelection || o is Eraser)))
                    {
                        if (drawObject != null)
                        {
                            drawObject.AddToDrawingContext(drawingContext);
                            drawingContext.Pop();
                        }
                    }
                }

                _bitmap.Render(drawingVisual);
                Image.Source = _bitmap;
            }
        }

        private void border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void border_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }

        private void border_TouchDown(object sender, TouchEventArgs e)
        {

        }

        private void border_TouchUp(object sender, TouchEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }
    }
}
