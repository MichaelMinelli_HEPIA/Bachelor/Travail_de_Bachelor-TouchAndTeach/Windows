﻿using System.Windows;
using System.Windows.Controls;
using TouchAndTeachWindows.Comm;
using TouchAndTeachWindows.Draw;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Logique d'interaction pour NotificationList.xaml
    /// </summary>
    public partial class AvailableDrawsList : UserControl
    {
        public AvailableDrawsList()
        {
            InitializeComponent();
            AvailableDrawsManager availableDrawsManager = AvailableDrawsManager.GetAvailableDrawsManager();
            AvailableDrawsListBox.ItemsSource = availableDrawsManager.GetAvailableDrawCollection();
        }

        private void AvailableDrawsElement_Pressed(object sender, RoutedEventArgs e)
        {
        }

        private void ScrollViewer_ManipulationBoundaryFeedback(object sender, System.Windows.Input.ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
