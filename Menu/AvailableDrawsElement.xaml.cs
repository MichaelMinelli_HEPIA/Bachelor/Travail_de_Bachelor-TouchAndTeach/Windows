﻿using Newtonsoft.Json;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TouchAndTeachWindows.Comm;
using TouchAndTeachWindows.Draw;
using TouchAndTeachWindows.Draw.DrawObjects;
using TouchAndTeachWindows.Utils;
using Page = TouchAndTeachWindows.Draw.Page;
using Point = System.Windows.Point;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Logique d'interaction pour NotificationElement.xaml
    /// </summary>
    public partial class AvailableDrawsElement : UserControl
    {
        #region Events Declaration
        public static readonly RoutedEvent PressedEvent = EventManager.RegisterRoutedEvent("AvailableDrawsElementPressed",
            RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(MenuButton));

        public event RoutedEventHandler Pressed
        {
            add { AddHandler(PressedEvent, value); }
            remove { RemoveHandler(PressedEvent, value); }
        }

        private readonly RoutedEventArgs _pressedArgs = new RoutedEventArgs(PressedEvent);
        #endregion

        private readonly DrawController _drawController = DrawController.GetDrawController();
        private readonly AvailableDrawsManager _availableDrawsManager = AvailableDrawsManager.GetAvailableDrawsManager();

        public AvailableDrawsElement()
        {
            InitializeComponent();
        }

        private void UserControl_TouchUp(object sender, TouchEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }

        private void UserControl_TouchDown(object sender, TouchEventArgs e)
        {
            e.Handled = true;
        }

        private void Dismiss_Pressed(object sender, RoutedEventArgs e)
        {
            if (!(DataContext is AvailableDraw)) return;
            AvailableDraw draw = (AvailableDraw)DataContext;
            _availableDrawsManager.RemoveDraw(draw);
        }

        private void Rename_Pressed(object sender, RoutedEventArgs e)
        {
            if (!(DataContext is AvailableDraw)) return;
            AvailableDraw draw = (AvailableDraw)DataContext;
            draw.rename();
        }

        private void Accept_Pressed(object sender, RoutedEventArgs e)
        {
            AvailableDraw draw = (AvailableDraw)DataContext;
            DrawController.GetDrawController().AddUserDrawing(draw.GetPage());
        }

        private void Dismiss_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
