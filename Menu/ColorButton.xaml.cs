﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Interaction logic for ColorButton.xaml
    /// </summary>
    public partial class ColorButton : UserControl
    {
        #region Events Declaration
        public static readonly RoutedEvent PressedEvent = EventManager.RegisterRoutedEvent("ColorButtonPressed",
            RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ColorButton));

        public event RoutedEventHandler Pressed
        {
            add { AddHandler(PressedEvent, value); }
            remove { RemoveHandler(PressedEvent, value); }
        }

        private readonly RoutedEventArgs _pressedArgs = new RoutedEventArgs(PressedEvent);
        #endregion

        private SolidColorBrush _color = new SolidColorBrush(Colors.Black);
        public SolidColorBrush Color
        {
            get { return _color; }
            set
            {
                _color = value;
                Rectangle.Fill = value;
            }
        }

        public ColorButton()
        {
            InitializeComponent();
            Rectangle.Fill = _color;
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }

        private void UserControl_TouchUp(object sender, TouchEventArgs e)
        {
            RaiseEvent(_pressedArgs);
            e.Handled = true;
        }

        private void UserControl_TouchDown(object sender, TouchEventArgs e)
        {
            e.Handled = true;
        }
    }
}
