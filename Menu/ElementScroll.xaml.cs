﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TouchAndTeachWindows.Menu
{
    /// <summary>
    /// Interaction logic for ElementScroll.xaml
    /// </summary>
    public partial class ElementScroll : UserControl
    {

        private Boolean _manipulated = false;
        private Point _mousePos;
        private Boolean _mouseMovingScroll = false;

        public ElementScroll()
        {
            InitializeComponent();
        }

        public int Count
        {
            get { return StackPanelElement.Children.Count; }
        }

        public void AddElement(UIElement element)
        {
            StackPanelElement.Children.Add(element);
        }

        public void InsertElement(UIElement element, int index)
        {
            StackPanelElement.Children.Insert(index, element);
        }

        public void RemoveElement(UIElement element)
        {
            if (StackPanelElement.Children.Contains(element))
                StackPanelElement.Children.Remove(element);
        }

        public void RemoveAllElements()
        {
            StackPanelElement.Children.Clear();
        }

        private void ScrollViewer_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void StackPanelElement_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {
            _manipulated = false;
            e.Handled = true;
        }

        private void StackPanelElement_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            if (Math.Abs(e.DeltaManipulation.Translation.X) > 3)
                _manipulated = true;
            else
                e.Handled = true;
        }

        private void UserControl_TouchUp(object sender, TouchEventArgs e)
        {
            //e.Handled = true;
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //e.Handled = true;
        }

        private void StackPanelElement_MouseMove(object sender, MouseEventArgs e)
        {
            Point pos = e.GetPosition(this);
            if (_mouseMovingScroll || _mousePos.X - pos.X > 10)
            {
                _mouseMovingScroll = true;
            }
            else
                e.Handled = true;
        }

        private void StackPanelElement_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _mousePos = e.GetPosition(this);
            e.Handled = true;
        }

        private void StackPanelElement_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _mouseMovingScroll = false;
        }
    }
}
