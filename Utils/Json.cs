﻿using System;
using System.Windows;
using System.Windows.Media;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TouchAndTeachWindows.Draw.DrawObjects;

namespace TouchAndTeachWindows.Utils
{

    public class JsonUtils
    {
        //on serialize, do NOT provide the DrawObjectConverter!! only on deserialize
        public static JsonConverter[] SerializeConverter = {new PointConverter(), new SolidColorBrushConverter()};
        public static JsonConverter[] DeserializeConverters = {new PointConverter(), new SolidColorBrushConverter(), new DrawObject.DrawObjectConverter()};
    }

    //source: http://stackoverflow.com/a/8031283/2405397
    public abstract class JsonCreationConverter<T> : JsonConverter
    {
        /// <summary>
        /// Create an instance of objectType, based properties in the JSON object
        /// </summary>
        /// <param name="objectType">type of object expected</param>
        /// <param name="jObject">
        /// contents of JSON object that will be deserialized
        /// </param>
        /// <returns></returns>
        protected abstract T Create(Type objectType, JObject jObject);

        public override bool CanConvert(Type objectType)
        {
            return typeof(T).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            try
            {
                // Load JObject from stream
                JObject jObject = JObject.Load(reader);

                // Create target object based on JObject
                T target = Create(objectType, jObject);

                // Populate the object properties
                serializer.Populate(jObject.CreateReader(), target);

                return target;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }

    class PointConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            Point point = (Point) value;
            serializer.Serialize(writer, new JObject { { "X", point.X }, { "Y", point.Y } });
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jObject = serializer.Deserialize<JObject>(reader);
            return new Point((double)jObject["X"], (double)jObject["Y"]);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Point);
        }
    }

    class SolidColorBrushConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            SolidColorBrush solidColorBrush = (SolidColorBrush) value;
            Color color = solidColorBrush.Color;
            Int32 colorArgb = (color.A << 24) | (color.R << 16) | (color.G << 8) | color.B;
            writer.WriteValue(colorArgb);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            long color = (long)reader.Value;
            return new SolidColorBrush(Color.FromArgb((byte)(color >> 24),
                    (byte)(color >> 16),
                    (byte)(color >> 8),
                    (byte)(color)));
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(SolidColorBrush);
        }
    }
}
