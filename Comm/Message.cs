﻿using TouchAndTeachWindows.Draw;

namespace TouchAndTeachWindows.Comm
{
    public class Message
    {
        public Device SenderDevice { get; set; }
        public Page Page { get; set; }

        public Message(Device senderDevice, Page page)
        {
            SenderDevice = senderDevice;
            Page = page;
        }
    }
}
