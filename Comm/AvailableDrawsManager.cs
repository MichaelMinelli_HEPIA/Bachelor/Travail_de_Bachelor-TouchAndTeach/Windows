﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Newtonsoft.Json;
using TouchAndTeachWindows.Draw;
using TouchAndTeachWindows.Draw.DrawObjects;
using TouchAndTeachWindows.Utils;
using System.Collections.Generic;

namespace TouchAndTeachWindows.Comm
{
    [JsonObject(MemberSerialization.OptIn)]
    public class AvailableDrawsManager
    {
        public event Action<Device, Message> MessageArrived = (device, message) => { }; 
        public event Action NoMoreMessage = () => { };

        [JsonProperty("AvailableDraws")]
        private readonly ObservableCollection<AvailableDraw> _availableDrawCollection;
        public ObservableCollection<AvailableDraw> GetAvailableDrawCollection() { return _availableDrawCollection; }

        private static AvailableDrawsManager _availableDrawsManager;
        public static AvailableDrawsManager GetAvailableDrawsManager()
        {
            return _availableDrawsManager ?? (_availableDrawsManager = new AvailableDrawsManager());
        }

        private AvailableDrawsManager()
        {
            _availableDrawCollection = new ObservableCollection<AvailableDraw>();
            BindingOperations.EnableCollectionSynchronization(_availableDrawCollection, this); //make _messageCollection thread safe
        }

        public void AddDraw(AvailableDraw draw)
        {
            _availableDrawCollection.Add(draw);
            removeWithoutName();
        }

        public void RemoveDraw(AvailableDraw draw)
        {
            if (draw == null) return;
            _availableDrawCollection.Remove(draw);
        }

        public AvailableDraw getDraw(string drawName)
        {
            foreach (AvailableDraw draw in _availableDrawCollection)
            {
                if (draw.drawName == drawName)
                    return draw;
            }

            return null;
        }

        public bool isAlreadyExist(string drawName)
        {
            foreach (AvailableDraw draw in _availableDrawCollection)
            {
                if (draw.drawName == drawName)
                    return true;
            }

            return false;
        }
        
        public void removeWithoutName()
        {
            List<AvailableDraw> toDelete = new List<AvailableDraw>();

            foreach (AvailableDraw draw in _availableDrawCollection)
                if (draw.drawName == "")
                    toDelete.Add(draw);

            foreach (AvailableDraw draw in toDelete)
                RemoveDraw(draw);
        }
    }
}
