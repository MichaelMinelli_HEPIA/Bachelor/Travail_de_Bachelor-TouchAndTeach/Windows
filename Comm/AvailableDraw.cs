﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;
using TouchAndTeachWindows.Draw;
using TouchAndTeachWindows.Draw.DrawObjects;

namespace TouchAndTeachWindows.Comm
{
    [JsonObject(MemberSerialization.OptIn)]
    public class AvailableDraw : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [JsonProperty("drawName")]
        public string drawName { get; set; }
        public List<DrawObject> drawObjects { get; set; }

        public Page GetPage()
        {
            Page page = new Page(drawName);

            foreach (DrawObject draw in drawObjects)
            {
                page.AddInactiveObject(draw);
            }

            page.ResetUUIDs();

            return page;
        }

        public AvailableDraw(List<DrawObject> drawObjects)
        {
            this.drawObjects = new List<DrawObject>(drawObjects);

            drawName = "";
            rename();
        }

        public void rename() 
        {
            //Dialog box creation
            System.Drawing.Size size = new System.Drawing.Size(500, 200);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.StartPosition = FormStartPosition.CenterParent;
            inputBox.Text = "Name of the shared draw";

            TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 10, 40);
            textBox.Location = new System.Drawing.Point(5, 100);
            textBox.Text = drawName;
            inputBox.Controls.Add(textBox);

            Label message = new Label();
            message.Size = new System.Drawing.Size(size.Width - 10, 40);
            message.Location = new System.Drawing.Point(5, 5);
            message.Text = "Please enter the name of this shared draw.";
            inputBox.Controls.Add(message);

            Label information = new Label();
            information.Size = new System.Drawing.Size(size.Width - 10, 40);
            information.Location = new System.Drawing.Point(5, 50);
            Font defaultFont = System.Drawing.SystemFonts.DefaultFont;
            information.Font = new Font(defaultFont.FontFamily, defaultFont.Size, System.Drawing.FontStyle.Bold);
            information.ForeColor = Color.Red;
            information.Text = "";
            inputBox.Controls.Add(information);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(100, 40);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 120 - 120, 150);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(100, 40);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 120, 150);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            //While the name is incorrect ask to the user
            DialogResult result;
            string temp;
            bool isAlreadyExist = false;
            do
            {
                result = inputBox.ShowDialog();
                temp = textBox.Text;

                isAlreadyExist = AvailableDrawsManager.GetAvailableDrawsManager().isAlreadyExist(temp) && temp != drawName;

                //Define the complementary text
                if (temp == "")
                {
                    information.Text = "The name cannot be empty.";
                }
                else if (isAlreadyExist)
                {
                    information.Text = "This name already exist in shared draws. Please enter a new name.";
                }

            } while (result == DialogResult.OK && (temp == "" || isAlreadyExist));

            //Change the name
            if (result == DialogResult.OK)
            {
                drawName = temp;
                RaisePropertyChanged("drawName");
            }
        }

        protected void RaisePropertyChanged(string propertyName = "")
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
