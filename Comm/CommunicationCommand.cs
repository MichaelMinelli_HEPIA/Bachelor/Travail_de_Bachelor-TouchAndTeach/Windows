﻿using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using TouchAndTeachWindows.Draw;
using TouchAndTeachWindows.Draw.DrawObjects;
using TouchAndTeachWindows.Utils;

namespace TouchAndTeachWindows.Comm
{
    class CommunicationCommand
    {
        public const String SendConnectionStatus = "SendConnectionStatus";
        public const String SendUserName = "SendUserName";
        public const String SendTextMessage = "SendTextMessage";
        public const String SendOnePage = "SendOnePage";
        public const String SendOneDrawObject = "SendOneDrawObject";
        public const String SendOneJpeg = "SendOneJpeg";
        public const String SendOneJpegDone = "SendOneJpegDone";
        public const String AskOnePage = "AskOnePage";
        public const String AskAllPages = "AskAllPages";

        public const String AskAvailableDraws = "AskAvailableDraws";
        public const String AvailableDraws = "AvailableDraws";
        public const String GetDraw = "GetDraw";
        public const String Draw = "Draw";

        public const String SendDocument = "SendDocument";

        public const String Separator = ":";

        private readonly String _command;
        private CommunicationCommand(String command)
        {
            _command = command;
        }

        public String GetCommand() { return _command; }

        public static CommunicationCommand CreateCommandSendTextMessage(String message)
        {
            return new CommunicationCommand(SendTextMessage + Separator + message);
        }

        public static CommunicationCommand CreateCommandSendConnectionStatus(String status)
        {
            return new CommunicationCommand(SendConnectionStatus + Separator + status);
        }

        public static CommunicationCommand CreateCommandSendOnePage(Page page)
        {
            //return new CommunicationCommand(SendOnePage + Separator + page.Serialize());
            return null;
        }

        public static CommunicationCommand CreateCommandSendOneDrawObject(DrawObject drawObject)
        {
            //return new CommunicationCommand(SendOneDrawObject + Separator + drawObject.Serialize());
            return null;
        }

        public static CommunicationCommand CreateCommandAskAllSlides()
        {
            return new CommunicationCommand(AskAllPages + Separator);
        }

        public static CommunicationCommand CreateCommandAvailableDraws()
        {
            return new CommunicationCommand(AvailableDraws + Separator + JsonConvert.SerializeObject(AvailableDrawsManager.GetAvailableDrawsManager(), Formatting.None, JsonUtils.SerializeConverter));
        }

        public static CommunicationCommand CreateCommandDraw(AvailableDraw draw)
        {
            return new CommunicationCommand(Draw + Separator + JsonConvert.SerializeObject(draw.GetPage(), Formatting.None, JsonUtils.SerializeConverter));
        }

        public static CommunicationCommand CreateCommandSendDocument(string size)
        {
            return new CommunicationCommand(SendDocument + Separator + size);
        }

        public static CommunicationCommand HandleMessageReceived(Device sender, String messageReceived)
        {
            int sepPos = messageReceived.IndexOf(Separator);
            string commandName = messageReceived.Substring(0, sepPos);
            string commandData = messageReceived.Substring(sepPos+1);
            if (commandName.Equals(SendUserName))
            {
                //TODO: if DeviceManager.Mode is restricted, we cannot receive the username. It's would be better if we could
                sender.UserName = commandData;

                return CreateCommandSendTextMessage(null);
            }
            if (commandName.Equals(SendOnePage))
            {
                MessageManager.GetMessageManager().AddMessage(sender, commandData);
                return CreateCommandSendTextMessage("slide successfully transmitted");
            }
            if (commandName.Equals(SendOneDrawObject))
            {
                //DrawObject newDrawObject = DrawObject.CreateFromString(commandData);
                //DrawController.GetDrawController().AddDrawObjectToCurrentPage(newDrawObject);
                //return CreateCommandSendTextMessage("");
            }
            if (commandName.Equals(AskOnePage))
            {
                return CreateCommandSendOnePage(DrawController.GetDrawController().GetCurrentPage());
            }
            if (commandName.Equals(SendConnectionStatus))
            {
                DevicesManager devicesManager = DevicesManager.GetDevicesManager();
                devicesManager.RemoveDevice(sender);

                return CreateCommandSendTextMessage(null);
            }
            if (commandName.Equals(SendOneJpeg))
            {
                return CreateCommandSendTextMessage("JpegMode:" + commandData);
            }
            if (commandName.Equals(AskAvailableDraws))
            {
                return CreateCommandAvailableDraws();
            }
            if (commandName.Equals(GetDraw))
            {
                AvailableDraw draw = AvailableDrawsManager.GetAvailableDrawsManager().getDraw(commandData);

                if (draw != null)
                {
                    return CreateCommandDraw(draw);
                }
                else
                {
                    return CreateCommandSendTextMessage(null);
                }
            }
            return CreateCommandSendTextMessage("Err");
        }
    }
}
