﻿#pragma checksum "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6FD6C2A41D9C902D25FCF0C8BB8272EE"
//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace TouchAndTeachWindows.Draw.UserDrawingUI {
    
    
    /// <summary>
    /// UserDrawingMenu
    /// </summary>
    public partial class UserDrawingMenu : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 10 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid grid;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border PostitBorder;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border TransparentBorder;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border MergeBorder;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border CloseBorder;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/TouchAndTeachWindows;component/draw/userdrawingui/userdrawingmenu.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
            ((TouchAndTeachWindows.Draw.UserDrawingUI.UserDrawingMenu)(target)).MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.UserDrawingMenu_OnMouseLeftButtonUp);
            
            #line default
            #line hidden
            
            #line 9 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
            ((TouchAndTeachWindows.Draw.UserDrawingUI.UserDrawingMenu)(target)).TouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.UserDrawingMenu_OnTouchUp);
            
            #line default
            #line hidden
            return;
            case 2:
            this.grid = ((System.Windows.Controls.Grid)(target));
            
            #line 10 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
            this.grid.MouseLeave += new System.Windows.Input.MouseEventHandler(this.Grid_OnMouseLeave);
            
            #line default
            #line hidden
            
            #line 10 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
            this.grid.TouchLeave += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.Grid_OnTouchLeave);
            
            #line default
            #line hidden
            return;
            case 4:
            this.PostitBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 5:
            this.TransparentBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 6:
            this.MergeBorder = ((System.Windows.Controls.Border)(target));
            
            #line 37 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
            this.MergeBorder.TouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.MergeBorder_OnTouchUp);
            
            #line default
            #line hidden
            
            #line 37 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
            this.MergeBorder.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.MergeBorder_OnMouseLeftButtonUp);
            
            #line default
            #line hidden
            return;
            case 7:
            this.CloseBorder = ((System.Windows.Controls.Border)(target));
            
            #line 40 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
            this.CloseBorder.TouchUp += new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.CloseBorder_OnTouchUp);
            
            #line default
            #line hidden
            
            #line 40 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
            this.CloseBorder.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.CloseBorder_OnLeftButtonUp);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            System.Windows.EventSetter eventSetter;
            switch (connectionId)
            {
            case 3:
            eventSetter = new System.Windows.EventSetter();
            eventSetter.Event = System.Windows.UIElement.TouchEnterEvent;
            
            #line 16 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
            eventSetter.Handler = new System.EventHandler<System.Windows.Input.TouchEventArgs>(this.EventSetter_OnHandler);
            
            #line default
            #line hidden
            ((System.Windows.Style)(target)).Setters.Add(eventSetter);
            eventSetter = new System.Windows.EventSetter();
            eventSetter.Event = System.Windows.UIElement.MouseEnterEvent;
            
            #line 17 "..\..\..\..\Draw\UserDrawingUI\UserDrawingMenu.xaml"
            eventSetter.Handler = new System.Windows.Input.MouseEventHandler(this.EventSetter_OnHandler);
            
            #line default
            #line hidden
            ((System.Windows.Style)(target)).Setters.Add(eventSetter);
            break;
            }
        }
    }
}

